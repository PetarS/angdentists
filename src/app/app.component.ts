import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  mapSwitchActive = false;

  onMapSwitchClicked() {
    this.mapSwitchActive = !this.mapSwitchActive;
  }
}

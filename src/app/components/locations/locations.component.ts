import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss']
})
export class LocationsComponent implements OnInit {
  @Input() mapActive: boolean;

  locations = [
    {
      name: 'Location 1',
      location: 1,
      address: 'Some Address bb',
      lat: '44.648744',
      lng: '20.943987',
      photos: [
        ('assets/images/temporary/img1.jpg'),
        ('assets/images/temporary/img2.jpg'),
        ('assets/images/temporary/img3.jpg')
      ],
      phone: '026 655 527',
      mobile_phone: '064 372 61 31',
      email: 'simicp87@gmail.com',
      website: 'www.website.com'
    },
    {
      name: 'Location 2',
      location: 2,
      address: 'Some Address bb',
      lat: '44.648744',
      lng: '20.943987',
      photos: [
        ('assets/images/temporary/img2.jpg'),
      ],
      phone: '026 655 527',
      mobile_phone: '064 372 61 31',
      email: 'simicp87@gmail.com',
      website: 'www.website.com'
    },
    {
      name: 'Location 3',
      location: 3,
      address: 'Some Address bb',
      lat: '44.648744',
      lng: '20.943987',
      photos: [
        ('assets/images/temporary/img3.jpg'),
      ],
      phone: '026 655 527',
      mobile_phone: '064 372 61 31',
      email: 'simicp87@gmail.com',
      website: 'www.website.com'
    },
    {
      name: 'Location 4',
      location: 4,
      address: 'Some Address bb',
      lat: '44.648744',
      lng: '20.943987',
      photos: [
        ('assets/images/temporary/img4.jpg'),
      ],
      phone: '026 655 527',
      mobile_phone: '064 372 61 31',
      email: 'simicp87@gmail.com',
      website: 'www.website.com'
    },
    {
      name: 'Location 5',
      location: 5,
      address: 'Some Address bb',
      lat: '44.648744',
      lng: '20.943987',
      photos: [
        ('assets/images/temporary/img5.jpg'),
      ],
      phone: '026 655 527',
      mobile_phone: '064 372 61 31',
      email: 'simicp87@gmail.com',
      website: 'www.website.com'
    },
    {
      name: 'Location 6',
      location: 6,
      address: 'Some Address bb',
      lat: '44.648744',
      lng: '20.943987',
      photos: [
        ('assets/images/temporary/img6.jpg'),
      ],
      phone: '026 655 527',
      mobile_phone: '064 372 61 31',
      email: 'simicp87@gmail.com',
      website: 'www.website.com'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}

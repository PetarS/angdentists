import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Output() mapSwitchClicked = new EventEmitter<void>();
  mapSwitchActive = false;

  constructor() { }

  ngOnInit() {
  }

  onMapSwitchClicked() {
    this.mapSwitchActive = !this.mapSwitchActive;
    this.mapSwitchClicked.emit();
  }

}
